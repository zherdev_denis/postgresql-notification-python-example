## PostgreSQL native notification example
python 3.7 <

Simple implementation PostgreSQL native notifications using tirgger and functions.

### How to use

- `docker-compose up -d`
- `virtualenv env`
- `source env/bin/activate` or `source env/Script/activate`
- `pip install -r requirements.txt`
- `python main.py` - filling the database with table, function and trigger
- `python sender.py` - separated connection emulation, inserting in table (firing the trigger)