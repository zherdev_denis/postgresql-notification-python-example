import select
import psycopg2.extensions

import constants

conn = psycopg2.connect(dbname=constants.DB_NAME,
                        user=constants.DB_USER,
                        password=constants.DB_PASSWORD,
                        host=constants.DB_HOST)
conn.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)

curs = conn.cursor()
curs.execute("LISTEN {};".format(constants.CHANNEL_NAME))

curs.execute("CREATE TABLE test_table (id serial PRIMARY KEY, message text)")

curs.execute("CREATE FUNCTION send_notification() "
             "RETURNS trigger AS $trigger$ "
             "BEGIN "  
             "NOTIFY test, 'hello'; "
             "RETURN NEW; "
             "END; "
             "$trigger$ LANGUAGE plpgsql;")

curs.execute("CREATE TRIGGER test_notify "
             "AFTER INSERT ON test_table "
             "FOR EACH ROW "
             "EXECUTE PROCEDURE send_notification();")

print("Waiting for notifications on channel '{}'".format(constants.CHANNEL_NAME))
while True:
    if select.select([conn], [], [], 5) == ([], [], []):
        print("Timeout")
    else:
        conn.poll()
        while conn.notifies:
            notify = conn.notifies.pop(0)
            print("Got notification:", notify.pid, notify.channel, notify.payload)
