import psycopg2.extensions

import constants

conn = psycopg2.connect(dbname=constants.DB_NAME,
                        user=constants.DB_USER,
                        password=constants.DB_PASSWORD,
                        host=constants.DB_HOST)
conn.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)

curs = conn.cursor()
curs.execute("INSERT INTO test_table(id, message) VALUES (1, 'message');")
